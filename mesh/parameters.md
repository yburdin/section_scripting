# Параметры сетки

```python
class MeshParameters:
  elementOrder: int = 1
  method: Enum
  globalElementSize: float
  globalMinElementSize: float | None
  globalMaxElementSize: float | None
  restraints: list[Restraint]
```

```python
class Restraint:
  restraintObject: tuple[Enum, int]
  elementSize: float | None
  numberOfDivisions: int | None
```

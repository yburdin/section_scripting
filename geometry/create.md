# Создание геометрических объектов

- [Точки](#точки)
  - [createPoint](#createpoint)
- [Кривые](#кривые)
  - [createLine](#createline)
  - [createCircleArc](#createcirclearc)
  - [createFillet](#createfillet)
  - [createSpline](#createspline)
  - [createCurveLoop](#createcurveloop)
- [Поверхности](#поверхности)
  - [createPlaneSurface](#createplanesurface)
  - [createRectangle](#createrectangle)
  - [createDisk](#createdisk)
  - [createPolygon](#createpolygon)

## Точки

### createPoint

```python
createPoint(
    x: float, 
    y: float,
) -> int
```

#### Описание

Функция `createPoint` создает объект типа "точка" и
возвращает индекс созданного объекта.

#### Параметры

- x (float): Координата X точки.
- y (float): Координата Y точки.

#### Возвращаемое значение

- int: Индекс созданного объекта точки.

## Кривые

### createLine

```python
createLine(
    startPoint: int,
    endPoint: int,
) -> int
```

#### Описание

Функция `createLine` создает объект типа "линия" и
возвращает индекс созданного объекта.

#### Параметры

- startPoint (int): Индекс начальной точки линии.
- endPoint (int): Индекс конечной точки линии.

#### Возвращаемое значение

- int: Индекс созданного объекта линии.

### createCircleArc

```python
createCircleArc(
    startPoint: int,
    endPoint: int, 
    centerPoint: int,
) -> int
```

#### Описание

Функция `createCircleArc` создает объект типа "дуга окружности" и
возвращает индекс созданного объекта.

#### Параметры

- startPoint (int): Индекс начальной точки дуги.
- endPoint (int): Индекс конечной точки дуги.
- centerPoint (int): Индекс точки центра окружности.

#### Возвращаемое значение

- int: Индекс созданного объекта дуги окружности.

### createFillet

```python
createFillet(
    pointId: int,
    radius: float, 
) -> int
```

#### Описание

Функция `createFillet` создает скругление между двумя линиями.

#### Параметры

- pointId (int): Индекс точки, общей для двух линий,
между которыми необходимо создать скругление.
- radius (float): Радиус скругления.

#### Возвращаемое значение

- int: Индекс созданного скругления - объекта дуги окружности.

### createSpline

```python
createSpline(
    points: list[int],
) -> int
```

#### Описание

Функция `createSpline` создает объект типа "сплайн" и
возвращает индекс созданного объекта.

#### Параметры

- points (list[int]): Список индексов точек, через которые проходит сплайн.

#### Возвращаемое значение

- int: Индекс созданного объекта сплайна.

### createCurveLoop

```python
createCurveLoop(
    curves: list[int],
) -> int
```

#### Описание

Функция `createCurveLoop` создает объект типа "цикл кривых" и
возвращает индекс созданного объекта.

#### Параметры

- curves (list[int]): Список индексов объектов кривых, образующих цикл.

#### Возвращаемое значение

- int: Индекс созданного объекта цикла кривых.

## Поверхности

### createPlaneSurface

```python
createPlaneSurface(
    outerLoop: int, 
    innerLoops: list[int] | None,
) -> int
```

#### Описание

Функция `createPlaneSurface` создает объект типа "плоская поверхность" и
возвращает индекс созданного объекта.

#### Параметры

- outerLoop (int): Индекс объекта цикла кривых, являющегося внешней границей поверхности.
- innerLoops (list[int] | None): Список индексов объектов циклов кривых,
являющихся внутренними границами поверхности.
Может быть None, если внутренних границ нет.

#### Возвращаемое значение

- int: Индекс созданного объекта плоской поверхности.

### createRectangle

```python
createRectangle(
    x: float,
    y: float,
    dx: float,
    dy: float,
) -> int
```

#### Описание

Функция `createRectangle` создает объект типа "прямоугольник" и
возвращает индекс созданного объекта.

#### Параметры

- x (float): Координата X нижнего левого угла прямоугольника.
- y (float): Координата Y нижнего левого угла прямоугольника.
- dx (float): Ширина прямоугольника.
- dy (float): Высота прямоугольника.

#### Возвращаемое значение

- int: Индекс созданного объекта прямоугольника.

### createDisk

```python
createDisk(
    xc: float,
    yc: float,
    radius: float,
) -> int
```

#### Описание

Функция `createDisk` создает объект типа "диск" и
возвращает индекс созданного объекта.

#### Параметры

- xc (float): Координата X центра диска.
- yc (float): Координата Y центра диска.
- radius (float): Радиус диска.

#### Возвращаемое значение

- int: Индекс созданного объекта диска.

### createPolygon

```python
createPolygon(
    xc: float,
    yc: float,
    radius: float,
    n: int,
) -> int
```

#### Описание

Функция `createPolygon` создает объект типа "многоугольник" и
возвращает индекс созданного объекта.

#### Параметры

- xc (float): Координата X центра описанной окружности многоугольника.
- yc (float): Координата Y центра описанной окружности многоугольника.
- radius (float): Радиус описанной окружности многоугольника.
- n (int): Количество сторон многоугольника.

#### Возвращаемое значение

- int: Индекс созданного объекта многоугольника.

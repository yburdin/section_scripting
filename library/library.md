# Работа с библиотекой

- [Создание новой библиотеки](#создание-новой-библиотеки)
  - [createSectionLibrary](#createsectionlibrary)
- [Загрузка библиотеки в текущую модель](#загрузка-библиотеки-в-текущую-модель)
  - [loadSectionLibrary](#loadsectionlibrary)
- [Удаление библиотеки из текущей модели](#удаление-библиотеки-из-текущей-модели)
  - [deleteSectionLibrary](#deletesectionlibrary)

## Создание новой библиотеки

### createSectionLibrary

```python
createSectionLibrary(
  name: str,
  description: str,
  path: Path,
) -> int
```

#### Описание

Функция `createSectionLibrary` создает новую библиотеку
и возвращает ее индекс в текущей модели.

#### Параметры

- name (str): Название новой библиотеки.
- description (str): Описание новой библиотеки.
- path (Path): Путь к месту сохранения новой библиотеки.

#### Возвращаемое значение

- int: Индекс созданной библиотеки.

## Загрузка библиотеки в текущую модель

### loadSectionLibrary

```python
loadSectionLibrary(
  path: Path,
) -> int
```

#### Описание

Функция `loadSectionLibrary` загружает библиотеку в текущую модель
и возвращает ее индекс в текущей модели.

#### Параметры

- path (Path): Путь к файлу библиотеки, которая будет загружена в модель.

#### Возвращаемое значение

- int: Индекс загруженной библиотеки в текущей модели.

## Удаление библиотеки из текущей модели

### deleteSectionLibrary

```python
deleteSectionLibrary(
  sectionLibraryId: int,
) -> None
```

#### Описание

Функция `deleteSectionLibrary` удаляет библиотеку из текущей модели.

#### Параметры

- sectionLibraryId (int): Индекс библиотеки в текущей модели,
которую нужно удалить.

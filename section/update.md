# Редактирование объектов

- [Обычные сечения](#обычные-сечения)
  - [addSurface](#addsurface)
  - [deleteSurface](#deletesurface)
- [Тонкостенные сечения](#тонкостенные-сечения)
  - [addCurve](#addcurve)
  - [deleteCurve](#deletecurve)
  - [setSectionThickness](#setsectionthickness)
  - [setCurveThickness](#setcurvethickness)
- [Общие функции](#общие-функции)
  - [setName](#setname)
  - [deleteSection](#deletesection)

## Обычные сечения

### addSurface

```python
addSurface(
  targetSection: int,
  targetSurface: int,
) -> None
```

#### Описание

Функция `addSurface` добавляет новую поверхность в указанный объект сечения.

#### Параметры

- targetSection (int): Индекс целевого объекта сечения,
в который необходимо добавить поверхность.
- targetSurface (int): Индекс целевой поверхности,
которую необходимо добавить в сечение.

### deleteSurface

```python
deleteSurface(
  targetSection: int,
  targetSurface: int,
) -> None
```

#### Описание

Функция `deleteSurface` удаляет указанную поверхность из указанного объекта сечения.

#### Параметры

- targetSection (int): Индекс целевого объекта сечения,
из которого необходимо удалить поверхность.
- targetSurface (int): Индекс целевой поверхности,
которую необходимо удалить из сечения

## Тонкостенные сечения

### addCurve

```python
addCurve(
  targetSection: int,
  targetCurve: int,
) -> None
```

#### Описание

Функция `addCurve` добавляет новую кривую в указанный объект тонкостенного сечения.

#### Параметры

- targetSection (int): Индекс целевого объекта сечения,
в который необходимо добавить кривую.
- targetCurve (int): Индекс целевой кривой,
которую необходимо добавить в сечение.

### deleteCurve

```python
deleteCurve(
  targetSection: int,
  targetCurve: int,
) -> None
```

#### Описание

Функция `deleteCurve` удаляет указанную кривую из указанного объекта сечения.

#### Параметры

- targetSection (int): Индекс целевого объекта сечения,
из которого необходимо удалить кривую.
- targetCurve (int): Индекс целевой кривой,
которую необходимо удалить из сечения.

### setSectionThickness

```python
setSectionThickness(
  targetSection: int,
  thickness: float,
) -> None
```

#### Описание

Функция `setSectionThickness` устанавливает толщину для всех кривых
указанного объекта сечения.

#### Параметры

- targetSection (int): Индекс целевого объекта сечения,
для которого необходимо установить толщину.
- thickness (float): Значение толщины сечения.

### setCurveThickness

```python
setCurveThickness(
  targetSection: int,
  targetCurve: int,
  thickness: float,
) -> None
```

#### Описание

Функция `setCurveThickness` устанавливает толщину для указанной кривой
в указанном объекте сечения.

#### Параметры

- targetSection (int): Индекс целевого объекта сечения,
в котором находится кривая.
- targetCurve (int): Индекс целевой кривой,
для которой необходимо установить толщину.
- thickness (float): Значение толщины кривой.

## Общие функции

### setName

```python
setName(
  targetSection: int,
  name: str,
) -> None
```

#### Описание

Функция `setName` устанавливает имя указанного объекта сечения.

#### Параметры

- targetSection (int): Индекс целевого объекта сечения,
для которого необходимо установить имя.
- name (str): Новое имя объекта сечения.

### deleteSection

```python
deleteSection(
  targetSection: int,
) -> None
```

#### Описание

Функция `deleteSection` удаляет указанный объект сечения.

#### Параметры

- targetSection (int): Индекс целевого объекта сечения,
который необходимо удалить

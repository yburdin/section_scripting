# Информация об объектах

- [Обычные сечения](#обычные-сечения)
  - [getSurfaces](#getsurfaces)
- [Тонкостенные сечения](#тонкостенные-сечения)
  - [getCurves](#getcurves)
  - [getCurveThickness](#getcurvethickness)
- [Общие функции](#общие-функции)
  - [getName](#getname)
  - [getSectionProperties](#getsectionproperties)
- [Характеристики сечения](#характеристики-сечения)
  - [CrossSectionProperties](#crosssectionproperties)
  - [EffectiveCrossSectionProperties](#effectivecrosssectionproperties)

## Обычные сечения

### getSurfaces

```python
getSurfaces(
  sectionId: int,
) -> list[int]
```

#### Описание

Функция `getSurfaces` возвращает список индексов поверхностей сечения.

#### Параметры

- sectionId (int): Индекс сечения.

#### Возвращаемое значение

- list[int]: Список индексов поверхностей, образующих сечение.

## Тонкостенные сечения

### getCurves

```python
getCurves(
  sectionId: int,
) -> list[int]
```

#### Описание

Функция `getCurves` возвращает список индексов кривых сечения.

#### Параметры

- sectionId (int): Индекс сечения.

#### Возвращаемое значение

- list[int]: Список индексов кривых, образующих сечение.

### getCurveThickness

```python
getCurveThickness(
  curveId: int,
) -> float
```

#### Описание

Функция `getCurveThickness` возвращает значение толщины кривой по ее индексу.

#### Параметры

- curveId (int): Индекс кривой.

#### Возвращаемое значение

- float: Значение толщины кривой.

## Общие функции

### getName

```python
getName(
  sectionId: int,
) -> str
```

#### Описание

Функция `getName` возвращает строку, представляющую название сечения,
по индексу сечения.

#### Параметры

- sectionId (int): Индекс сечения.

#### Возвращаемое значение

- str: Название сечения.

### getSectionProperties

```python
getSectionProperties(
  sectionId: int,
) -> CrossSectionProperties
```

#### Описание

Функция `getSectionProperties` возвращает для сечения, заданного индексом,
объект `CrossSectionProperties`, содержащий различные свойства,
такие как площадь, момент инерции и радиус инерции.

#### Параметры

- sectionId (int): Индекс сечения.

#### Возвращаемое значение

- CrossSectionProperties: объект, содержащий свойства сечения.

## Характеристики сечения

### CrossSectionProperties

```python
class CrossSectionProperties:
  area: float
  section_center_x: float
  section_center_y: float
  inertia_moment_x: float
  inertia_moment_y: float
  inertia_moment_xy: float
  alpha_0: float
  inertia_moment_u: float
  inertia_moment_v: float
  moment_of_area_x: float
  moment_of_area_y: float
  shear_center_x: float
  shear_center_y: float
  shear_center_u: float
  shear_center_v: float
  torsional_constant: float
  warping_constant: float
  section_modulus_u_top: float
  section_modulus_u_bot: float
  section_modulus_v_top: float
  section_modulus_v_bot: float
  section_modulus_u_max: float
  section_modulus_u_min: float
  section_modulus_v_max: float
  section_modulus_v_min: float
  plastic_section_modulus_u: float
  plastic_section_modulus_v: float
  gyration_radius_x: float
  gyration_radius_y: float
  gyration_radius_u: float
  gyration_radius_v: float
  f_omega_u: float
  f_omega_v: float
```

### EffectiveCrossSectionProperties

```python
class EffectiveCrossSectionProperties(CrossSectionProperties):
  eccentricity_x: float
  eccentricity_y: float
```
